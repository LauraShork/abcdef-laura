import java.time.LocalTime;
import java.util.Map;
import java.util.TreeMap;
import java.util.Set;

public class Data
{
   private final Map<String, Location> locationMap = new TreeMap<>();
   private final Map<String, Route>    routeMap    = new TreeMap<>();

   public Data()
   {
      /// === Locations A ... F ========
      var location = new Location( "A" );
      locationMap.put( location.getName(), location );

      location = new Location( "B" );
      locationMap.put( location.getName(), location );

      location = new Location( "C" );
      locationMap.put( location.getName(), location );

      location = new Location( "D" );
      locationMap.put( location.getName(), location );

      location = new Location( "E" );
      locationMap.put( location.getName(), location );

      location = new Location( "F" );
      locationMap.put( location.getName(), location );

      /// === Routes A-B-C-D ========
      for (int hour = 7; hour <= 19; hour += 2)
      {
         var departure = LocalTime.of( hour, 0 );
         var route     = new Route( locationMap.get( "A" ), departure );
         route.addStopOver( locationMap.get( "B" ), LocalTime.of( hour, 15 ), LocalTime.of( hour, 16 ) );
         route.addStopOver( locationMap.get( "C" ), LocalTime.of( hour, 31 ), LocalTime.of( hour, 33 ) );
         route.addEndPoint( locationMap.get( "D" ), LocalTime.of( hour, 48 ) );
         routeMap.put( route.getKey(), route );
         //System.out.println(route.getKey());
      }

      // === Routes D-C-B-A
      for (int hour = 7; hour <= 19; hour += 2)
      {
         var departure = LocalTime.of( hour, 0 );
         var route     = new Route( locationMap.get( "D" ), departure );
         route.addStopOver( locationMap.get( "C" ), LocalTime.of( hour, 15 ), LocalTime.of( hour, 16 ) );
         route.addStopOver( locationMap.get( "B" ), LocalTime.of( hour, 31 ), LocalTime.of( hour, 33 ) );
         route.addEndPoint( locationMap.get( "A" ), LocalTime.of( hour, 48 ) );
         routeMap.put( route.getKey(), route );
      }

      /// === Routes E-B-C-F ========
      for (int hour = 8; hour <= 17; hour += 1)
      {
         var departure  = LocalTime.of (hour, 30);
         var route      = new Route( locationMap.get( "E" ), departure);
         route.addStopOver( locationMap.get( "B" ), LocalTime.of( hour, 45), LocalTime.of( hour, 46) );
         route.addStopOver( locationMap.get( "C" ), LocalTime.of( hour++, 1), LocalTime.of( hour, 3) );
         route.addEndPoint( locationMap.get( "F" ), LocalTime.of( hour, 18) );
         routeMap.put( route.getKey(), route );
      }

      // === Routes F-C-B-E ========
      for (int hour = 8; hour <= 17; hour += 1)
      {
         var departure  = LocalTime.of (hour, 30);
         var route      = new Route( locationMap.get( "F" ), departure);
         route.addStopOver( locationMap.get( "C" ), LocalTime.of( hour, 45), LocalTime.of( hour, 46) );
         route.addStopOver( locationMap.get( "B" ), LocalTime.of( hour++, 1), LocalTime.of( hour, 3) );
         route.addEndPoint( locationMap.get( "E" ), LocalTime.of( hour, 18) );
         routeMap.put( route.getKey(), route );
      }

      // === Route B-C ===
      for (int hour = 12; hour <= 12; hour += 1)
      {
         var departure = LocalTime.of(hour, 0);
         var route     = new Route( locationMap.get( "B" ), departure);
         route.addEndPoint(locationMap.get( "C" ), LocalTime.of(hour, 15));
         routeMap.put( route.getKey(), route );
      }
      // === Route C-B ===
      for (int hour = 12; hour <= 12; hour += 1)
      {
         var departure = LocalTime.of(hour, 0);
         var route     = new Route( locationMap.get( "C" ), departure);
         route.addEndPoint(locationMap.get( "B" ), LocalTime.of(hour, 15));
         routeMap.put( route.getKey(), route );
      }
   }
   public void writeRoutes()
   {
      for (var entry : routeMap.values())
      {
         routeMap.get(entry.getKey()).write();
      }
   }
   public void writeRoutesABCDE()
   {
      Set<String> keys = routeMap.keySet();
      for(String key : keys)
      {
         if(key.startsWith("A-B-C-D")){
            var route = routeMap.get(key);
            route.write();
      }
      } //volgende opdracht heeft contains nodig
   }
}